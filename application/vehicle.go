package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-interpreter/model"
)

func (app *Application) HandleVehicleRegistered(ctx context.Context, event *model.Event) error {
	data := new(model.VehicleRegisteredEvent)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	vehicle := model.VehicleRegistered{
		ID:           event.SubjectIDs[0],
		Brand:        data.Model,
		LicensePlate: data.NumberPlate,
	}

	if err := app.Request(ctx, http.MethodPost, "/vehicles", vehicle, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleVehicleOwnerDetermined(ctx context.Context, event *model.Event) error {
	data := new(model.VehicleOwnerDeterminedEvent)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	owner := model.VehicleOwnerDetermined{
		PersonID:   data.PersonID,
		AssignedAt: event.OccurredAt,
	}

	if err := app.Request(ctx, http.MethodPost, fmt.Sprintf("/vehicles/%s/purchase", event.SubjectIDs[0]), owner, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}
