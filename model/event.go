package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID           uuid.UUID       `json:"id"`
	RegisteredAt time.Time       `json:"registeredAt"`
	OccurredAt   time.Time       `json:"occurredAt"`
	SubjectIDs   []uuid.UUID     `json:"subjectIds"`
	EventType    string          `json:"eventType"`
	EventData    json.RawMessage `json:"eventData"`
}

type VehicleRegisteredEvent struct {
	Model       string `json:"model"`
	NumberPlate string `json:"numberPlate"`
}

type VehicleRegistered struct {
	ID           uuid.UUID `json:"id"`
	Brand        string    `json:"brand"`
	LicensePlate string    `json:"licensePlate"`
}

type VehicleOwnerDeterminedEvent struct {
	PersonID uuid.UUID `json:"person"`
}

type VehicleOwnerDetermined struct {
	PersonID   uuid.UUID `json:"personId"`
	AssignedAt time.Time `json:"assignedAt"`
}
